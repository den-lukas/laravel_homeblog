<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BannersAndForeign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->id();
            $table->string("banner_title");
            $table->string("banner_save_path");
            $table->timestamps();
        });
        Schema::create('articles_has_authors', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('articles_id');
            $table->unsignedBigInteger('authors_id');

            $table->foreign('articles_id')
                ->references('id')
                ->on('articles')->
                onDelete("cascade");
            $table->foreign('authors_id')
                ->references('id')
                ->on('authors')
                ->onDelete("cascade");
        });
        Schema::create('categories_has_articles', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('categories_id');
            $table->unsignedBigInteger('articles_id');

            $table->foreign('categories_id')
                ->references('id')
                ->on('categories')->
                onDelete("cascade");
            $table->foreign('articles_id')
                ->references('id')
                ->on('articles')
                ->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banners');
        Schema::dropIfExists('articles_has_authors');
        Schema::dropIfExists('categories_has_articles');
    }
}
