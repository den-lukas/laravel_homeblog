@extends("layouts.compose")
@section("content")
    <div class="row mb-2">
        <h1>Articles</h1>
        <ul class="list-group">
            @foreach($articles as $article)
                <li class="list-group-item"><a href="{{route('articles.show', $article->id)}}">{{$article->title}}</a></li>
            @endforeach
        </ul>
        @auth
            <div class="pt-5">
                <buttom class="btn btn-primary"><a href="{{route('articles.create')}}">New article</a></buttom>
            </div>
        @endauth
    </div>
@endsection
