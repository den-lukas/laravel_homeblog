@extends("layouts.compose")
@section("content")
    <div class="p-4 p-md-5 mb-4 text-white rounded bg-dark">
        <div class="col-md-6 px-0">
            <form method="post" action="{{route('articles.update', $article)}}">
                @csrf
                @method('PUT')
                <div class="form-group col-md-15">
                    <label for="inputTitle">Title</label>
                    @error('title')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                    <input name="title" value="{{$article->title}}" class="form-control" id="inputTitle">
                </div>
                <div class="form-group col-md-15">
                    <label for="inputDescription">Description</label>
                    @error('description')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                    <input name="description" value="{{$article->description}}" class="form-control"
                           id="inputDescription">
                </div>
                <div class="form-group col-md-15">
                    <label for="inputArticle">Article</label>
                    @error('text')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                    <textarea name="text" value="" class="form-control" style="height: 150px" id="inputArticle" rows="10">{{$article->text}}</textarea>
                </div>
                <div class="form-group col-md-15">
                    <label for="inputSlug">Slug</label>
                    @error('slug')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                    <input name="slug" value="{{$article->slug}}" class="form-control" id="inputSlug">
                </div>
                <div class="form-group col-md-15">
                    <label for="inputIsActive">Is active</label>
                    @error('is_active')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                    <input name="is_active" value="{{$article->is_active}}" class="form-control" id="inputIsActive">
                </div>
                <div class="mt-3">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>

            @if(Auth::user()->isAdmin())
            <form class="mt-3" method="post" action="{{route('articles.destroy', $article)}}">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-primary">Delete</button>
            </form>
            @endif

            <div>
                <button class="btn btn-primary mt-3"><a href="{{route('articles.index')}}">Cancel</a></button>
            </div>
        </div>
    </div>
@endsection
