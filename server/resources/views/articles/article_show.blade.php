@extends("layouts.compose")
@section("content")
<div class="row mb-2">
    <h1>Article {{$article->title}}</h1>
    <h2>Description: {{$article->description}}</h2>
    <p>{{$article->text}}</p>
    @auth
        <div>
            <button  class="btn btn-primary"><a href="{{route('articles.edit', $article->id)}}">Edit</a></button>
        </div>
    @endauth
</div>
@endsection
