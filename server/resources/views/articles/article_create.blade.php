@extends("layouts.compose")
@section("content")
    <div class="p-4 p-md-5 mb-4 text-white rounded bg-dark">

        <div class="col-md-6 px-0">
            <form method="post" action="{{route('articles.store')}}">
                @csrf
                <div class="form-group col-md-15">
                    <label for="inputTitle">Title</label>
                    @error('title')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                    <input name="title" class="form-control" id="inputTitle" value="{{old('title')}}">
                </div>
                <div class="form-group col-md-15">
                    <label for="inputDescription">Description</label>
                    @error('description')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                    <input name="description" class="form-control" id="inputDescription" value="{{old('description')}}">
                </div>
                <div class="form-group col-md-15">
                    <label for="inputArticle">Article</label>
                    @error('text')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                    <textarea name="text" class="form-control" style="height: 150px" id="inputArticle" value="{{old('text')}}" rows="10"></textarea>
                </div>
                <div class="form-group col-md-15">
                    <label for="inputSlug">Slug</label>
                    @error('slug')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                    <input name="slug" class="form-control" id="inputSlug" value="{{old('slug')}}">
                </div>
                <div class="form-group col-md-15">
                    <label for="inputIsActive">Is active</label>
                    @error('is_active')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                    <input name="is_active" class="form-control" id="inputIsActive" value="{{old('is_active')}}">
                </div>
                <div class="mt-3">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button class="btn btn-primary"><a href="{{route('articles.index')}}">Cancel</a></button>
                </div>
            </form>


        </div>
    </div>
@endsection
