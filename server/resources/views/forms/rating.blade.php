@extends("layouts.compose")
@section("content")
<div class="p-4 p-md-5 mb-4 text-white rounded bg-dark">
    <div class="col-md-6 px-0">
        <form method="post" action="{{route('rating.create')}}">
            @csrf
            <h1>Rate this product</h1>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="rating"  value="1">
                <label class="form-check-label" for="inlineRadio1">1</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="rating"  value="2">
                <label class="form-check-label" for="inlineRadio2">2</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="rating"  value="3">
                <label class="form-check-label" for="inlineRadio2">3</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="rating"  value="4">
                <label class="form-check-label" for="inlineRadio2">4</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="rating"  value="5">
                <label class="form-check-label" for="inlineRadio2">5</label>
            </div>

            <div class="mt-3">
                <button type="submit" class="btn btn-primary">Rate</button>
            </div>
        </form>

    </div>
</div>
@endsection
