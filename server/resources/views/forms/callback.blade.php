@extends("layouts.compose")
@section("content")

    <div class="p-4 p-md-5 mb-4 text-white rounded bg-dark">
        <div class="col-md-6 px-0">
            <form method="post" action="{{route('callback.create')}}">
                @csrf
                <div class="form-group col-md-15">
                    <label for="inputPhone">Input your phone number to callback</label>
                    <input name="phone" class="form-control" id="inputPhone">
                </div>
                <div class="mt-5">
                    <button type="submit" class="btn btn-primary">Call me</button>
                    <button formaction="{{route('callback.destroy')}}" class="btn btn-primary">Don't call me</button>
                </div>
            </form>
        </div>
    </div>

@endsection
