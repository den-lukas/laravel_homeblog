@extends("layouts.compose")
@section("content")
    <div class="p-4 p-md-5 mb-4 text-white rounded bg-dark">
        <div class="col-md-6 px-0">
            <h2>Create profile</h2>
            <form method="post" action="{{route('profile.create')}}">
                @csrf
                <div class="form-group col-md-15">
                    <label for="inputName">Name</label>
                    <input name="name" class="form-control" >
                    <label for="inputPhone">Phone</label>
                    <input name="phone" class="form-control" >
                </div>
                <div class="mt-5">
                    <button type="submit" class="btn btn-primary">Create</button>
                </div>
            </form>
        </div>

        <div class="col-md-6 pt-5">
            <h2>Read profile</h2>
            <form method="get" action="{{route('profile.show')}}">
                @csrf
                <div class="form-group col-md-15">
                    <label for="inputName">Input name to take number</label>
                    <input name="name" class="form-control" >
                </div>
                <div class="mt-5">
                    <button type="submit" class="btn btn-primary">Read</button>
                </div>
            </form>
        </div>

        <div class="col-md-6 pt-5">
            <h2>Update profile</h2>
            <form method="post" action="{{route('profile.store')}}">
                @csrf
                <div class="form-group col-md-15">
                    <label for="inputOldName">Old Name</label>
                    <input name="oldName" class="form-control" >
                    <label for="inputName">New Name</label>
                    <input name="name" class="form-control" >
                    <label for="inputOldPhone">Old Phone</label>
                    <input name="oldPhone" class="form-control" >
                    <label for="inputPhone">New Phone</label>
                    <input name="phone" class="form-control" >
                </div>
                <div class="mt-5">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>

        <div class="col-md-6 pt-5">
            <h2>Delete profile</h2>
            <form method="post" action="{{route('profile.destroy')}}">
                @csrf
                <div class="form-group col-md-15">
                    <label for="inputName">Name</label>
                    <input name="name" class="form-control" >
                    <label for="inputPhone">Phone</label>
                    <input name="phone" class="form-control" >
                </div>
                <div class="mt-5">
                    <button type="submit" class="btn btn-primary">Delete</button>
                </div>
            </form>
        </div>

    </div>
@endsection
