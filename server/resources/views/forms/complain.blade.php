@extends("layouts.compose")
@section("content")
<div class="p-4 p-md-5 mb-4 text-white rounded bg-dark">
    <div class="col-md-6 px-0">
        <form method="post" action="{{route('complain.create')}}">
            @csrf
            <div class="form-group col-md-15">
                <label >Input Id</label>
                <input name="user_id" class="form-control">
            </div>
            <h2>Leave your complain</h2>
            <div class="form-floating">
                <textarea class="form-control" name="complain" id="floatingTextarea2" style="height: 100px"></textarea>
                <label for="floatingTextarea2" class="text-secondary">Complain</label>
            </div>
            <div class="mt-3">
                <button type="submit" class="btn btn-primary">Send</button>
                <button formaction="{{route('complain.destroy')}}" class="btn btn-primary">Delete complain</button>
            </div>
        </form>

    </div>
</div>
@endsection
