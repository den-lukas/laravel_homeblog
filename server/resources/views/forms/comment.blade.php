@extends("layouts.compose")
@section("content")
    <div class="p-4 p-md-5 mb-4 text-white rounded bg-dark">
        <div class="col-md-6 px-0">
            <form method="post" action="{{route('comment.create')}}">
                @csrf
                <div class="form-group col-md-15">
                    <label >Input Id</label>
                    <input name="article_id" class="form-control">
                </div>
                <h2>Leave your comment about product</h2>
                <div class="form-floating">
                    <textarea class="form-control" name="comment" id="floatingTextarea2" style="height: 100px"></textarea>
                    <label for="floatingTextarea2" class="text-secondary">Comment</label>
                </div>

                <div class="mt-3">
                    <button type="submit" class="btn btn-primary">Send</button>
                    <button formaction="{{route('comment.destroy')}}" class="btn btn-primary">Delete comment</button>
                </div>
            </form>
        </div>
    </div>
@endsection
