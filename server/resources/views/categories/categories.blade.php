@extends("layouts.compose")
@section("content")
<div class="row mb-2">
    <h1>Categories</h1>
    <ul class="list-group">
        @foreach($categories as $category)
            <li class="list-group-item"><a href="{{route('categories.show', $category->id)}}">{{$category->category}}</a></li>
        @endforeach
    </ul>
    @auth
        <div class="pt-5">
            <buttom class="btn btn-primary"><a href="{{route('categories.create')}}">New category</a></buttom>
        </div>
    @endauth

</div>
@endsection
