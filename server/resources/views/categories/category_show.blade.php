@extends("layouts.compose")
@section("content")
<div class="row mb-2">
    <h1>Category</h1>

    <ul class="list-group">
        <li class="list-group-item">Category: {{$category->category}} </li>
        <li class="list-group-item">Description: {{$category->description}}</li>
    </ul>
    @auth
        <div>
            <button  class="btn btn-primary"><a href="{{route('categories.edit', $category->id)}}">Edit</a></button>
        </div>
    @endauth

</div>
@endsection
