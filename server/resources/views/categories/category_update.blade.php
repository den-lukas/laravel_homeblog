@extends("layouts.compose")
@section("content")
    <div class="p-4 p-md-5 mb-4 text-white rounded bg-dark">
        <div class="col-md-6 px-0">
            <form method="POST" action="{{route('categories.update', $category)}}">
                @csrf
                @method('PUT')
                <div class="form-group col-md-15">
                    <label for="inputCategory">Category</label>
                    @error('category')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                    <input name="category" value="{{$category->category}}" class="form-control" id="inputCategory">
                </div>
                <div class="form-group col-md-15">
                    <label for="inputDescription">Description</label>
                    @error('description')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                    <input name="description" value="{{$category->description}}" class="form-control" id="inputDescription">
                </div>
                <div class="form-group col-md-15">
                    <label for="inputSlug">Slug</label>
                    @error('slug')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                    <input name="slug" value="{{$category->slug}}" class="form-control" id="inputSlug">
                </div>
                <div class="form-group col-md-15">
                    <label for="inputIsActive">Is active</label>
                    @error('is_active')
                    <div class="alert alert-danger">{{$message}}</div>
                    @enderror
                    <input name="is_active" value="{{$category->is_active}}" class="form-control" id="inputIsActive">
                </div>
                <div class="mt-3">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
            @if(Auth::user()->isAdmin())
                <form class="pt-3" method="post" action="{{route('categories.destroy', $category)}}">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-primary">Delete</button>
                </form>
            @endif


            <div>
                <button class="btn btn-primary mt-3"><a href="{{route('categories.index')}}">Cancel</a></button>
            </div>


        </div>
    </div>
@endsection
