<?php

use App\Http\Controllers\ArticleController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\Auth\SocialAuthFacebookController;
use App\Http\Controllers\Auth\SocialAuthGoogleController;
use Illuminate\Support\Facades\Route;
use App\Models\Category;
use App\Models\Article;
use App\Models\Author;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\RedirectController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\CallbackController;
use App\Http\Controllers\ComplaneController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\RatingController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();


Route::get('/', function () {
    return view('/content/main', ["pageTitle" => "Main"]);
})->name('main');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
//
//Route::get("/category/{id?}", function () {
//    return view("/content/category", ["pageTitle" => "Category"]);
//})->name("blog");
//
////Route::get("/categories", function () {
////    return view("/content/categories", ["pageTitle" => "Categories"]);
////});
//
//Route::get("/article/{id?}", function () {
//    return view("/content/article", ["pageTitle" => "Article"]);
//});
//
//Route::get("/author/{id?}", function () {
//    return view("/content/author", ["pageTitle" => "Author"]);
//});

Route::prefix('profile')->group(function () {
    Route::view('/', "forms.form_name_phone")->name("profile.index");
    Route::post('/save', [ProfileController::class, 'create'])->name("profile.create");
    Route::get('/read', [ProfileController::class, 'read'])->name("profile.show");
    Route::post('/update', [ProfileController::class, 'update'])->name("profile.store");
    Route::post('/delete', [ProfileController::class, 'delete'])->name("profile.destroy");
});


Route::prefix('callback')->group(function () {
    Route::view('/', "forms.callback",)->name("callback.index");
    Route::post('/save', [CallbackController::class, 'create'])->name("callback.create");
    Route::post('/delete', [CallbackController::class, 'delete'])->name("callback.destroy");
});

Route::prefix('complain')->group(function () {
    Route::view('/', "forms.complain")->name("complain.index");
    Route::post('/save', [ComplaneController::class, 'create'])->name("complain.create");
    Route::post('/delete', [ComplaneController::class, 'delete'])->name("complain.destroy");
});

Route::prefix('comment/')->group(function() {
    Route::view('/{id?}', "forms.comment")->name("comment.index");
    Route::post('/save', [CommentController::class, 'create'])->name("comment.create");
    Route::post('/delete', [CommentController::class, 'delete'])->name("comment.destroy");
});
Route::prefix('rating')->group(function () {
    Route::view('/{id?}', "forms.rating")->name("rating.index");
    Route::post('/save', [RatingController::class, 'create'])->name("rating.create");
});

Route::resource('admin/categories', CategoryController::class );

Route::resource('admin/articles', ArticleController::class );


Route::prefix("/auth")->group(function (){
    Route::get('/facebook/redirect', [SocialAuthFacebookController::class, 'redirect'])->name("facebook");
    Route::get('/facebook/callback', [SocialAuthFacebookController::class, 'callback']);

    Route::get('/google/redirect', [SocialAuthGoogleController::class, 'redirectToProvider'])->name("google");
    Route::get('/google/callback', [SocialAuthGoogleController::class, 'handleProviderCallback']);

});
