<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    protected $fillable = ["category", "description", "is_active", 'slug'];

//    public function article()
//    {
//        return $this->belongsToMany(Article::class, "categories_has_articles");
//    }

}
