<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;

    protected $fillable = ["title", "description", "text", "is_active", 'slug'];

//    public function category()
//    {
//        return $this->belongsToMany(Category::class, "categories_has_articles");
//    }
}

