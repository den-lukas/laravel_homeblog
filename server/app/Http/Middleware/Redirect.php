<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Http\Controllers\RedirectController;

class Redirect
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle(Request $request, Closure $next)
    {
        $redirect = \App\Models\Redirect::where("hash", $_SERVER["REQUEST_URI"])->first();
            if (!empty($redirect)) {
                return redirect($redirect->url);
            }
        return $next($request);
    }
}
