<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;

class ProfileController extends Controller
{
    public function create()
    {
        $profile = Profile::create([
            'name' => $_POST['name'],
            'phone' => $_POST['phone'],
        ]);
        return $profile->save();
    }

    public function read()
    {
        $profile = Profile::where("name", $_GET['name'])->value('phone');
        print_r($_GET['name'] . ' - ' . $profile);
    }

    public function update()
    {
        $id = Profile::where("name", $_POST['oldName'])->where("phone", $_POST['oldPhone'])->value('id');
        Profile::updateOrCreate(['id' => $id],['name' => $_POST['name'], 'phone' => $_POST['phone'],]);
    }

    public function delete()
    {
        Profile::where("name", $_POST['name'])->where("phone", $_POST['phone'])->delete();

    }
}
