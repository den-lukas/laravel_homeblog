<?php

namespace App\Http\Controllers;

use App\Models\Complain;
use Illuminate\Http\Request;

class ComplaneController extends Controller
{
    public function create()
    {
        $complain = Complain::create([
            'complain' => $_POST['complain'],
            'user_id' => $_POST['user_id'],
        ]);
        return $complain->save();
    }
    public function delete()
    {
        Complain::where('user_id', $_POST['user_id'])->delete();
    }
}
