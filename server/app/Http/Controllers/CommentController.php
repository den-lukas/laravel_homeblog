<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function create()
    {
        $complain = Comment::create([
            'comment' => $_POST['comment'],
            'article_id' => $_POST['article_id'],
        ]);
        return $complain->save();
    }
    public function delete()
    {
        Comment::where('article_id', $_POST['article_id'])->delete();
    }
}
