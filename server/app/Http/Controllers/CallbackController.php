<?php

namespace App\Http\Controllers;

use App\Models\Callback;
use Illuminate\Http\Request;

class CallbackController extends Controller
{
    public function create()
    {
        $callback = Callback::create([
            'phone' => $_POST['phone'],
        ]);
        return $callback->save();
    }
    public function delete()
    {
        Callback::where('phone', $_POST['phone'])->delete();

    }
}
